// Do this as the first thing so that any code reading it knows the right env.
process.env.NODE_ENV = "development";
process.env.REACT_APP_CLIENT_PLUGINS_USE_MOCK = true;

const path = require("path");
const fs = require("fs");
const webpack = require("webpack");
const WrmPlugin = require("atlassian-webresource-webpack-plugin");

// We keep our provided dependencies in a JSON file to easily track them. More on this below.
const providedDependenciesJSON = require("./provided-dependencies.json");

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const pluginDirectory = fs.realpathSync(process.cwd());
const frontendDirectory = path.join(pluginDirectory, "src", "main", "frontend");
function resolveFrontend(relativePath) {
  return path.resolve(frontendDirectory, relativePath);
}

// The build should go into the target directory, rather than the src
// __dirname returns directory that this file lives in and does not depend on location where node is called from.
const targetDirectory = path.join(__dirname, "target");

const config = {
  entry: {
    /** JS Code for WebPanel */
    webPanelEntrypoint: resolveFrontend("web-panel/index.tsx"),

    /** JS code for WebItems */
    webItemEntrypoint: resolveFrontend("web-item/index.tsx"),

    /** Entrypoints where the registration of each plugin happens */
    clientPlugin1Entrypoint: resolveFrontend("client-plugin-1/index.tsx"),
    clientPlugin2Entrypoint: resolveFrontend("client-plugin-2/index.ts"),
  },

  // Location of our code
  context: resolveFrontend("."),

  // We build our code inside the target/classes folder.
  output: path.join(targetDirectory, "classes")
};

/**
 * List of all dependencies provided by Jira that we are going to use as ES6 modules.
 *
 * This will do 2 things:
 * - Create the dependency entry in the generated XML for the apropiate resource to be ready when we need it using WRM.
 * - Let Webpack know of the AMD Module name of our dependency so that it can look for it in the AMD registry. Yeah,
 *   provided dependencies are mostly AMD modules (sometimes are just globals)
 */
const providedDependencies = new Map();
Object.entries(providedDependenciesJSON).forEach(([key, value]) => {
  providedDependencies.set(key, {
    dependency: value,
    import: {
      var: `require('${key}')`,
      amd: key
    }
  });
});

/**
 * Our very Basic Webpack configuration working with React
 * https://www.typescriptlang.org/docs/handbook/react-&-webpack.html
 */
module.exports = {
  mode: process.env.NODE_ENV,
  node: false,
  context: config.context,
  entry: config.entry,
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"]
  },
  output: {
    path: config.output,
    filename: "[name].bundle.js"
  },
  module: {
    rules: [
      { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
    ]
  },
  /**
   * Resolve packages from `dist`. Only for DEMO.
   */
  resolve: {
    alias: {
      "@atlassian/client-plugins": path.resolve(__dirname, '../dist'),
      "react": path.resolve(__dirname, 'node_modules', 'react')
    }
  },
  plugins: [
    /**
     * More info about WRM Webpack Plugin here:
     * https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin
     */
    new WrmPlugin({
      pluginKey: "com.atlassian.tec.stash-plugin",

      // Path where WRM Plugin will store the generated XML file for us.
      xmlDescriptors: path.resolve(
        path.join(targetDirectory, "classes"),
        "META-INF",
        "plugin-descriptors",
        "wr-webpack-bundles.xml"
      ),

      contextMap: {
        /**
         * The name of the Client Location is used as context for each plugin entrypoint.
         * At the moment of the Location registration, the Registry will request all resources for that given
         * context, and that will load and execute the registration of the Plugins.
         */
        clientPlugin1Entrypoint: ["reff.client-plugins-location"],
        clientPlugin2Entrypoint: ["reff.client-plugins-location"],
      },

      // Set the list of provided dependencies
      providedDependencies
    }),
    new webpack.DefinePlugin({
      "process.env": {
        REACT_APP_CLIENT_PLUGINS_USE_MOCK:
          process.env.REACT_APP_CLIENT_PLUGINS_USE_MOCK
      }
    })
  ]
};
