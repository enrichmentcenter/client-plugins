## Using this DEMO
DEMO created using `atlas-create-plugin`, and running in Refapp.

### Build Demo
After building the Lib, cd into `/demo` folder and run:

1. `mvn install package`, this will install all deps and build the Demo Frontend code.

### Running Demo
1. `yarn run mock`, to start the REST endpoint mock.
2. `atlas-run`, to start the demo.

### Consideration with this Demo
Since we're trying to create locations to render plugins and web-items using an Atlassian Plugin, and not directly modifying
the product (Reffapp in this case), we're using a regular WebPanel and a few JS resources to serve as the example of
how a product should implement the API.

`/resources/`
- `atlassian-plugin.xml`: regular Altassian Plugin declaration. Here's the declaration of the WebPanel used for Demoing.
- `web-panel.vm`: WebPanel's template, where the `#myPlugin` and `#myWebItems` divs are declared for demoing.

## Client Plugins implementation
There are two Plugin examples:

### Plugin (plugin devs)
`/frontend/client-plugin-1/`
- `index.tsx`: main entry-point for a Plugin declaration using React.

`/frontend/client-plugin-2/`
- `index.js`: main entry-point for a Plugin declaration using plain JS.

### Plugin Location (products)
`/frontend/web-panel/`
- `index.tsx`: React implementation of a Location provider for Plugins.

## Web Items with entry-point implementation

### Web Item entry-point definition (plugin devs)
`/resources/`
- `atlassian-plugin.xml`: web-item declaration using regular Atlassian WebFragments API, but adding a new element called `<entry-point>` to specify a Web Resource implementing the WebItems API for dynamic attributes using client context.
- `web-item-1-attributes.js`: JS file entry-point where the Plugin Dev implements the API to register attributes calculated using client context.

### WebItem Location (products)
`/frontend/web-item/`
- `index.tsx`: React implementation of a Location provider for Web Items.

