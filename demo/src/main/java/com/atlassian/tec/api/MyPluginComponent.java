package com.atlassian.tec.api;

public interface MyPluginComponent
{
    String getName();
}