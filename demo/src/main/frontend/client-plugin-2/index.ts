import { registry } from "@atlassian/client-plugins";

const PLUGIN_KEY = "com.atlassian.tec.client-plugin-demo:my-plugin-2";
const LOCATION = "reff.client-plugins-location";

class ClientPlugin2 {
    parentElm: HTMLElement;

    mount = (parentElm: HTMLElement, context: any) => {
        if (parentElm) {
            parentElm.innerHTML = `
        <h4>Plugin with Vanilla JS</h4>
        <p>Plugin: <b>${PLUGIN_KEY}</b></p>
        <p>
            Context.val: ${context.value} <br/>
            Context.greeting: ${context.greeting} <br/>
            Context.farewell: ${context.farewell} <br/>
        </p>
      `;
            this.parentElm = parentElm;
        }
    };

    unmount = () => {
        // Could detach to any event listener here. I'm just gonna clean up my parent element for now
        this.parentElm.innerHTML = "";
    };
}

registry.registerPlugin(PLUGIN_KEY, LOCATION, ClientPlugin2);
