import React, { useEffect } from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { registry } from "@atlassian/client-plugins";

const PLUGIN_KEY = "com.atlassian.tec.client-plugin-demo:my-plugin-1";
const LOCATION = "reff.client-plugins-location";

interface Props {
    context: any;
}

function MyComponent({ context }: Props) {
    useEffect(() => {
        function eventListener() {
            console.log(`Event Listener from ${PLUGIN_KEY} for val ${context.val}`);
        }

        document.addEventListener("my-event", eventListener);

        return function cleanUp() {
            document.removeEventListener("my-event", eventListener);
        };
    });

    return (
        <div>
            <h4>Plugin with React</h4>
            <p>
                Plugin: <b>{PLUGIN_KEY}</b>
            </p>
            <p>Context.val: {context.value}</p>
            <p>
                <button onClick={() => alert(context.greeting)}>Say Hi!</button>
                <button onClick={() => alert(context.farewell)}>Say Bye!</button>
            </p>
        </div>
    );
}

/**
 * Class defining our Client Plugin. Note that it looks very similar to a Web Component, which is the reason why I would
 * like to investigate how to replace this for a Web Component.
 */
class ClientPlugin1 {
    parentElm: HTMLElement;

    mount = (parentElm: HTMLElement, context: any) => {
        if (parentElm) {
            render(<MyComponent context={context} />, parentElm);
            this.parentElm = parentElm;
        }
    };

    unmount = () => {
        unmountComponentAtNode(this.parentElm);
    };
}

registry.registerPlugin(PLUGIN_KEY, LOCATION, ClientPlugin1);
