import React, { useState } from "react";
import { render } from "react-dom";
import { useWebItems, WebItemLocation } from "@atlassian/client-plugins";

const container = document.querySelector("#myWebItems");
const headerStyles = {
    marginTop: "30px"
};
const listItemStyles = {
    margin: "20px 0",
    border: "2px solid blue"
};

function WebItemImpl() {
    const [value, setValue] = useState(0);
    const [isLocationHidden, setIsLocationHidden] = useState(false);
    const webItems = useWebItems("reff.web-items-location", { value });

    return (
        <React.Fragment>
            <h2 style={headerStyles}>Web Items with React</h2>

            <p>Current value: {value}</p>
            <button onClick={() => setValue(value - 1)}>-1</button>
            <button onClick={() => setValue(value + 1)}>+1</button>

            <p>Show/Hide Client Locations (testing re-rendering)</p>
            <button onClick={() => setIsLocationHidden(!isLocationHidden)}>{isLocationHidden ? "Show" : "Hide"}</button>

            {isLocationHidden ? null : (<React.Fragment>

                {/* web-items hook implementation */}
                <h3>Web Item location with hooks</h3>
                <ul>
                    {webItems.map(webItem => (
                        <li key={webItem.key} style={listItemStyles}>
                            <button {...webItem.attributes}>{webItem.label}</button>
                        </li>
                    ))}
                </ul>

                {/* web-items component implementation */}
                <h3>Web Item location with location component</h3>
                <WebItemLocation name="reff.web-items-location" context={{ value }}>
                    {webItems => (
                        <ul>
                            {webItems.map(webItem => (
                                <li key={webItem.key} style={listItemStyles}>
                                    <button {...webItem.attributes}>{webItem.label}</button>
                                </li>
                            ))}
                        </ul>
                    )}
                </WebItemLocation>
            </React.Fragment>)}
        </React.Fragment>
    );
}

/** This codes runs after the WebPanel is rendered */
render(<WebItemImpl />, container);
