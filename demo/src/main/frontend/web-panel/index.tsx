import React, { useState } from "react";
import { render } from "react-dom";
import { useWebPanels, WebPanelLocation } from "@atlassian/client-plugins";

declare global {
    interface Window { React1: any; }
}

const container = document.querySelector("#myPlugin");
const headerStyles = {
    marginTop: "30px"
};
const valueContainerStyles = {
    margin: "20px 0",
    border: "2px solid red"
};

function WebPanelImpl() {
    const context = {
        greeting: "Hello World 😁!",
        farewell: "Bye Bye cruel world 😖!"
    };

    const [isLocationHidden, setIsLocationHidden] = useState(false);
    const webPanels = useWebPanels("reff.client-plugins-location", context);

    return (
        <React.Fragment>
            <h2 style={headerStyles}>Client Plugins</h2>

            <p>Show/Hide Client Locations (testing re-rendering)</p>
            <button onClick={() => setIsLocationHidden(!isLocationHidden)}>{isLocationHidden ? "Show" : "Hide"}</button>

            {isLocationHidden ? null : (<React.Fragment>
                {/* web-panel hook implementation */}
                <h3>Client Plugin location with hooks</h3>
                {webPanels.map((webPanel, n) => (
                    <div style={valueContainerStyles} key={n}>
                        {webPanel}
                    </div>
                ))}

                {/* web-panel component implementation */}
                <h3>Client Plugin location with location component</h3>
                <WebPanelLocation name="reff.client-plugins-location" context={{ ...context }}>
                    {webPanelDescriptors => webPanelDescriptors.map((webPanel, n) => (
                        <div style={valueContainerStyles} key={n}>
                            {webPanel}
                        </div>
                    ))}
                </WebPanelLocation>
            </React.Fragment>)}
        </React.Fragment>
    );
}

/** This codes runs after the WebPanel is rendered */
render(<WebPanelImpl />, container);
