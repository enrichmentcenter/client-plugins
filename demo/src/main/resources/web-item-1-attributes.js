// Ideally, we would like plugin devs to just export the function implementing the API
// and our platform converting that into the register... part.
// export {callback} ;

window["__client_plugins_registry"].registerWebItemCallback(
    "com.atlassian.tec.client-plugin-demo:web-item-1",
    function (context) {
        return {
            hidden: !context.value,
            disabled: context.value > 5
        };
    }
);
