package ut.com.atlassian.tec;

import org.junit.Test;
import com.atlassian.tec.api.MyPluginComponent;
import com.atlassian.tec.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}