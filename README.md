# Client Plugins API

Front End implementation of the project [Better client-side plugins](https://hello.atlassian.net/wiki/spaces/TEC/pages/277428992/Better+client-side+plugins)

## DEMO
Go to [Demo folder](demo/) to learn more about implementing this Spec and see it in action.

## Installing the API in a Product
Follow the [Dev Installation Guide](https://hello.atlassian.net/wiki/spaces/TEC/pages/536335790/Client+Plugin+-+Dev+Installation+Guide)

## Implementing a Location (Product Developers)

### Location for a ClientPlugin
- Import the provided react components to create a Location point and render a client Plugin:
```jsx
    import { ClientLocation, ClientPlugin, PluginTypes } from "@atlassian/client-plugins";
```
- Create a Location point in your product, and pass some context when rendering the plugins in that location:
```jsx
    <ClientLocation name="my-location" type={PluginTypes.clientPlugin}>
        {clientPluginDescriptors => plugins.map(plugin => (
            <div>
                <ClientPlugin plugin={plugin} context={context} />
            </div>
        ))}
    </ClientLocation>
```

### Location for a WebItem
- Import the provided react components to create a Location point:
```jsx
    import { ClientLocation, PluginTypes } from "@atlassian/client-plugins";
```
- Create a Location point in your product, and pass some context to the location:
```jsx
    <ClientLocation name="my-location" type={PluginTypes.webItem} context={context}>
        {webItemDescriptors => webItemDescriptors.map(webItem => (
            <button {...webItem.attributes}>{webItem.label}</button>
        ))}
    </ClientLocation>
```
- Attributes in a WebItem are calculated by the API, but it doesn't have any restrictions at the moment, which means Plugin Devs can set any attribute name they desire. It's Products responsibility to filter the attritbues they would not like to support.

## Declaring Plugins and Web Items (Plugin Developers)

### Create a Client Plugin
- Create a Class with your `mount` and `unmount` methods
```js
    class MyClientPlugin {

        /**
         * This method will be called when your plugin is about to be rendered.
            * @param parentElm is a DOM element that will serve as the parent of your plugin. You can render what you want in there
            * @param context is a piece of information that the product shares with your plugin at the moment of rendering
            */
        mount(parentElm, context) {
            ...
        }

        /**
         * This method will be called when your plugin parent is beign destroyed. Do your clean up operations in here.
            */
        unmount() {
            ...
        }
    }
```
- And register your plugin using:
```js
    window["__client_plugins_registry"].registerPlugin("your-plugin-key", "product-provided-location", MyClientPlugin);
```
- Finally, you need to define your entrypoint as a web-resource in your Atlassian Plugin definition:
```xml
    <web-resource key="entrypoint-myClientPlugin">
        <!-- The spec uses the name of the location as context to load this resource at the right time -->
        <context>product-provided-location</context>

        <!-- Add your entrypoint as a resource -->
        <resource type="download" name="myClientPlugin.js" location="myClientPlugin.js"/>
    </web-resource>
```

### Create an attribute callback for Web Items
- Create a JS file implementing the attributes callback as follow:
```js
// The web-items callback receives a context as parameter
function getAttributes(context) {
    // and should return an object with the attributes you would like to bind to the web-item
    return {
        hidden: !context.value,
        disabled: context.value > 5,
        onClick: function() {
            alert("You cliked me!");
        }
    };
}
```
- And register your web-item callback using:
```js
    window["__client_plugins_registry"].registerWebItemCallback("web-item-key", getAttributes);
```
- Finally, you need to define your `<web-item>` with its `<entry-point>` and `<web-resource>`:
```xml
    <web-item key="web-item-key" section="product-provided-location">
        <label key="My Awesome Web Item" />
        <!-- The entry-point should have the name of a web-resource containing the JS file with your callback -->
        <entry-point>web-item-key-attributes</entry-point>
    </web-item>

    <web-resource key="web-item-key-attributes">
        <resource type="download" location="web-item-key-attributes.js" name="web-item-key-attributes.js" />
    </web-resource>
```

## Development

### Build the Lib
In root folder, run:

1. `yarn install`
2. `yarn build`

This will create a `dist` folder with the compile lib.

### API code
All API related code is in:

`/lib/`
- `registry.ts`: Main registry for Plugins and Locations.
- `ClientLocation.tsx`: React Component to register a Location. Its use is optional, but convenient if you're already using React.
- `ClientPlugin.tsx`: React Component to render a Plugin. Its use is optional, but makes rendering Client Plugins much easier since it matches React life cycles with the Plugins mount/unmount methods, and also provides a Error Boundary to avoid crashing the app if something fails while rendering the plugin.
- `types.ts`: is where all the exported and shared interfaces are defined.