import { useState, useEffect, useMemo } from 'react';
import registry from './registry';
import { WebItemDescriptor } from './types';

function filterDescriptors(descriptors: WebItemDescriptor[], context: any) {
    return descriptors.reduce(
        (filteredDescriptors, descriptor) => {
            const descriptorProps = typeof descriptor.callback === 'function' ? descriptor.callback(context) : {};

            if (!descriptorProps.hidden) {
                filteredDescriptors.push({
                    ...descriptor,
                    attributes: descriptorProps,
                });
            }

            return filteredDescriptors;
        },
        [] as WebItemDescriptor[]
    );
}

/**
 * @param name
 * @param context
 * @returns WebItemDescriptors[]
 */
export default function useWebItems(name: string, context: any) {
    const [descriptors, setDescriptors] = useState([]);

    useEffect(() => {
        const subscription = registry
            .getLocation(name, 'web-item')
            .subscribe(registeredDescriptors => setDescriptors(registeredDescriptors));

        return () => subscription.unsubscribe();
    }, []);

    const memoizedDescriptors = useMemo(() => filterDescriptors(descriptors, context), [descriptors, context]);

    return memoizedDescriptors;
}
