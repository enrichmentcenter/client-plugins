import React, { useState, useEffect, useMemo } from 'react';
import registry from './registry';
import ClientPlugin from './ClientPlugin';

/**
 * @param name
 * @param context
 * @returns JSX.Element[]
 */
function useWebPanels(name: string, context: any) {
    const [descriptors, setDescriptors] = useState([]);

    useEffect(() => {
        const subscription = registry.getLocation(name, 'plugin').subscribe(registeredDescriptors => setDescriptors(registeredDescriptors));

        return () => subscription.unsubscribe();
    }, []);

    const memoizedDescriptors = useMemo(() => {
        return descriptors.map(descriptor => <ClientPlugin key={descriptor.key} plugin={descriptor} context={context} />);
    }, [descriptors, context]);

    return memoizedDescriptors;
}

export default useWebPanels;
