import { LocationObservable, LocationObserver, LocationSubscription } from './types';

/**
 * Subject for a single location and its observers
 *
 * Observers are callbacks provided by the different instances of a location that want to be notified
 * when a list of descriptors changed and is available to be rendered.
 */
export default class LocationsSubject<T> {
    private observers: LocationObserver<T>[] = [];

    // Behave as a ReplaySubject, where we return the last value sent to previous observers, if there was any, on subscribe.
    private lastDescriptors: T[];

    /**
     * Notify all observers with a list of descriptors. Send the last value sent if no value is provided.
     * @param [descriptors] descriptors to send to the observers
     */
    notify = (descriptors?: T[]) => {
        this.lastDescriptors = descriptors || this.lastDescriptors;

        this.observers.forEach(observer => observer(this.lastDescriptors));
    };

    /**
     * Add an observer to the location subject. The observer will be notified each time there's a new list of
     * descriptors ready to be renderer.
     * @param locationObserver
     */
    subscribe = (locationObserver: LocationObserver<T>): LocationSubscription => {
        this.observers.push(locationObserver);

        if (this.lastDescriptors) {
            locationObserver(this.lastDescriptors);
        }

        return {
            unsubscribe: () => this.unsubscribe(locationObserver),
        };
    };

    /**
     * Remove an observer from the location subject.
     * @param observer
     */
    unsubscribe = (observer: LocationObserver<T>) => {
        const observerIndex = this.observers.indexOf(observer);

        this.observers.splice(observerIndex, 1);
    };

    /**
     * Converts a Subject into an Observable.
     * An observable is a subset of a subject, but it can only subscribe to values, not notify new value.
     */
    asObservable = (): LocationObservable<T> => {
        return {
            subscribe: this.subscribe,
        };
    };
}
