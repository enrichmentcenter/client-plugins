export interface Descriptor {
    key: string;
    location: string;
}

export type LocationCallback<T> = (descriptors: T[]) => any;

export type LocationObserver<T> = (descriptors: T[]) => void;

export interface LocationSubscription {
    unsubscribe: () => void;
}

export interface LocationObservable<T> {
    subscribe: (locationObserver: LocationObserver<T>) => LocationSubscription;
}

export interface PluginDescriptor extends Descriptor {
    constructor: PluginConstructor;
}

export interface BaseWebItemDescriptor extends Descriptor {
    entryPoint: string;
    title: string;
    url: string;
    label: string;
}

interface WebItemAttributes {
    [key: string]: any;
}

export type WebItemCallback = (context: any) => WebItemAttributes;

export interface WebItemDescriptor extends BaseWebItemDescriptor {
    callback?: WebItemCallback;
    attributes?: WebItemAttributes;
}

export interface Plugin {
    mount: (parentElement: HTMLElement, context: any) => void;

    unmount: () => void;
}

export type PluginConstructor = new (...args: any[]) => Plugin;
