import usePlugins from './useWebPanels';
import { LocationCallback } from './types';

interface PluginLocationProps {
    name: string;
    context?: any;
    children?: LocationCallback<JSX.Element>;
}

function PluginLocation({ name, context, children }: PluginLocationProps) {
    const descriptors = usePlugins(name, context);

    if (typeof children !== 'function') {
        return descriptors;
    }

    return children(descriptors);
}

export default PluginLocation;
