import { LocationCallback, WebItemDescriptor } from './types';
import useWebItems from './useWebItems';

interface WebItemLocationProps {
    name: string;
    context?: any;
    children: LocationCallback<WebItemDescriptor>;
}

function WebItemLocation({ name, context, children }: WebItemLocationProps) {
    if (typeof children !== 'function') {
        console.error('ClientLocation: "children" is not a function');
        return null;
    }

    const descriptors = useWebItems(name, context);

    return children(descriptors);
}

export default WebItemLocation;
