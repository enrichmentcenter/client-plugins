import { Descriptor, PluginDescriptor, PluginConstructor, WebItemDescriptor, WebItemCallback, LocationObservable } from './types';

import LocationsSubject from './LocationSubject';

declare let WRM: any;

/**
 * Registry of Client Plugins and Client Locations
 */
class ClientPluginsRegistry {
    /**
     * Map of locations and their subjects.
     */
    locations: { [key: string]: LocationsSubject<Descriptor> } = {};

    /**
     * List of plugins registered.
     */
    plugins: PluginDescriptor[] = [];

    /**
     * List of Web Items already fetched.
     */
    webItems: WebItemDescriptor[] = [];

    /**
     * Get a Location observable. The observer will be notified when new descriptors are available to render.
     */
    // eslint-disable-next-line
    getLocation(locationName: string, type: 'plugin'): LocationObservable<PluginDescriptor>;
    // eslint-disable-next-line
    getLocation(locationName: string, type: 'web-item'): LocationObservable<WebItemDescriptor>;
    // eslint-disable-next-line
    getLocation(locationName: string, type: 'plugin' | 'web-item'): LocationObservable<PluginDescriptor | WebItemDescriptor | undefined> {
        if (!locationName) {
            console.warn('Registry: no location name specified.');
            return new LocationsSubject<undefined>().asObservable();
        }

        switch (type) {
            case 'web-item':
                return this.getWebItemsLocation(locationName);

            case 'plugin':
                return this.getClientPluginsLocation(locationName);

            default:
                console.warn('Registry: no location name specified.');
                return new LocationsSubject<undefined>().asObservable();
        }
    }

    /**
     * TODO: DEPRECATE in favor of AMD modules, entry-point transformer and a server declaration of this kind of
     * plugins to be fetched similar to WebItems.
     *
     * Plugins are just Web Resources that implement an SPI with mount/unmount exported methods.
     * This API is called by the resource to register such implementation.
     */
    registerPlugin(pluginKey: string, locationName: string, pluginConstructor: PluginConstructor) {
        if (!pluginKey || !locationName || !pluginConstructor) {
            console.warn('Invalid Client Plugin specified.');
            return;
        }

        const pluginDescriptor: PluginDescriptor = {
            key: pluginKey,
            location: locationName,
            constructor: pluginConstructor,
        };

        this.plugins.push(pluginDescriptor);

        // Only notify if the Subject was already.
        if (this.locations[locationName]) {
            this.locations[locationName].notify(this.getPluginsByLocation(locationName));
        }
    }

    /**
     * TODO: DEPRECATE in favor of AMD modules and entry-point transformer.
     *
     * WebItem's resources use this API to register their implementation of the getAttributes SPI.
     */
    registerWebItemCallback(key: string, callback: WebItemCallback) {
        const webItemDescriptor = this.webItems.find(webItem => webItem.key === key);

        if (!webItemDescriptor) {
            console.warn(`Registry: couldn't register web-item callback, no such web-item registered with the id ${key}`);
            return;
        }

        webItemDescriptor.callback = callback;

        // Only notify if the Subject was already.
        if (this.locations[webItemDescriptor.location]) {
            this.locations[webItemDescriptor.location].notify(this.getWebItemsByLocation(webItemDescriptor.location));
        }
    }

    /**
     * Registers the location of type "plugins".
     * TODO: implement a server side declaration of Plugins to be fetched as WebItems.
     */
    private getClientPluginsLocation(locationName: string): LocationObservable<PluginDescriptor> {
        if (this.locations[locationName]) {
            return this.locations[locationName].asObservable() as LocationObservable<PluginDescriptor>;
        }

        this.locations[locationName] = new LocationsSubject<PluginDescriptor>();

        WRM.require(`wrc!${locationName}`).then(() => this.locations[locationName].notify(this.getPluginsByLocation(locationName)));

        // Return the Observable right away and let WRM.require promise notify the LocationSubject when the descriptors are fetched.
        return this.locations[locationName].asObservable() as LocationObservable<PluginDescriptor>;
    }

    /**
     * Register the location of type "web-item", and fetches the metadata of WebItems declared for such location.
     */
    private getWebItemsLocation(locationName: string): LocationObservable<WebItemDescriptor> {
        if (!this.locations[locationName]) {
            this.locations[locationName] = new LocationsSubject<WebItemDescriptor>();

            // TODO: should we use fetch? what's the standard way of doing Ajax requests in Server products?
            fetch(this.getWebItemsRestURL(locationName))
                .then(res => res.json())
                .then(res => res.webItems || [])
                .then(webItemDescriptors => {
                    // Store the web-item descriptors in our registry
                    this.webItems.push(...webItemDescriptors);

                    // Prepare the list of web-resources to be loaded for each web-item
                    return this.flatWebItemResources(this.webItems);
                })
                .then(webItemResources => {
                    if (webItemResources.length) {
                        // If there are any resources to load, wait until we have load them before notifying the locations about the
                        // descriptors.
                        return WRM.require(webItemResources);
                    }

                    return Promise.resolve();
                })
                .catch(console.warn)
                // Notify to the LocationSubject when done.
                .then(() => this.locations[locationName].notify(this.getWebItemsByLocation(locationName)));
        }

        // Return the Observable right away and let the Fetch notify the LocationSubject when the descriptors are fetched.
        return this.locations[locationName].asObservable() as LocationObservable<WebItemDescriptor>;
    }

    private getWebItemsByLocation(locationName: string): WebItemDescriptor[] {
        return this.webItems.filter(descriptor => descriptor.location === locationName);
    }

    private getPluginsByLocation(locationName: string): PluginDescriptor[] {
        return this.plugins.filter(descriptor => descriptor.location === locationName);
    }

    private flatWebItemResources(descriptors: WebItemDescriptor[]) {
        return descriptors.reduce(
            (resources, descriptor) => (descriptor.entryPoint ? [...resources, descriptor.entryPoint] : resources),
            [] as string[]
        );
    }

    private getWebItemsRestURL(locationName: string) {
        const useMock = process.env.REACT_APP_CLIENT_PLUGINS_USE_MOCK;
        const baseURL = useMock ? `http://localhost:3000` : `${WRM.contextPath()}/rest/client-plugins/1.0/client-plugins`;

        return `${baseURL}/items?location=${locationName}`;
    }
}

const REGISTRY_NAMESPACE = '__client_plugins_registry';

if (!(window as any)[REGISTRY_NAMESPACE]) {
    // CREATE THE REGISTRY ON LOAD, ONLY IF IT DOESN'T ALREADY EXISTS
    (window as any)[REGISTRY_NAMESPACE] = new ClientPluginsRegistry();
}

export default (window as any)[REGISTRY_NAMESPACE] as ClientPluginsRegistry;
