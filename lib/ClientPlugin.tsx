import React from 'react';
import { PluginDescriptor, Plugin } from './types';

interface Props {
    plugin: PluginDescriptor;
    context?: any;
}

/**
 * React Wrapper Component for:
 * - Render a Plugin
 * - Hook mount/unmount Plugin methods to React lifecycles.
 * - Everytime a plugin re-renders, it will destroy and reconstruct the Plugin.
 *
 * This component can be the place for further optimization of plugin's re-render and clean-up
 */
class ClientPlugin extends React.Component<Props> {
    pluginInstance: Plugin;

    componentWillUnmount() {
        this.pluginInstance.unmount();
    }

    renderPlugin = (elm: HTMLElement) => {
        if (!elm) {
            // If the Elm Ref is empty, it means it's unmounting. Skip rendering
            // Read more here: https://reactjs.org/docs/refs-and-the-dom.html#caveats-with-callback-refs
            return;
        }

        /**
         * Unmount old instance of the component if this wrapper is re-rendering.
         */
        if (this.pluginInstance) {
            this.pluginInstance.unmount();
        }

        const { plugin, context } = this.props;

        /**
         * Create a new instance of the plugin to clean up old state, and then Mount in the new Ref
         */
        this.pluginInstance = new plugin.constructor();
        this.pluginInstance.mount(elm, context);
    };

    render() {
        /**
         * Tried to optimize the recreation of ref if not needed, but it wasn't destroying the Plugin if props changed.
         * We might need to implement an "update" lifecycle to optimize the mount/unmount of plugins.
         */
        return <span ref={elm => this.renderPlugin(elm)} />;
    }
}

interface ErrorBoundaryState {
    hasError: boolean;
}

/**
 * Error Boundary React Component
 *
 * Simplest example taken from: https://reactjs.org/docs/error-boundaries.html
 * Further improvement can be done here too!!
 */
function withErrorBoundary(WrappedComponent: React.ComponentClass) {
    return class ErrorBoundary extends React.Component<any, ErrorBoundaryState> {
        state: ErrorBoundaryState = {
            hasError: false,
        };

        static getDerivedStateFromError() {
            // Update state so the next render will show the fallback UI.
            return { hasError: true };
        }

        componentDidCatch() {
            // We could log the error to an error reporting service
        }

        render() {
            const { plugin } = this.props;
            const { hasError } = this.state;

            if (hasError) {
                // We could render any custom fallback UI
                return plugin ? (
                    <p>
                        Something went wrong initializing plugin<b>{plugin.key}</b>
                    </p>
                ) : (
                    <p>Something went wrong initializing a plugin</p>
                );
            }

            return <WrappedComponent {...this.props} />;
        }
    };
}

export default withErrorBoundary(ClientPlugin);
